<?php

namespace Rltsquare\Badar\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Afzel implements ArgumentInterface
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function customText(): string
    {
        return "Custom Text";
    }
}
