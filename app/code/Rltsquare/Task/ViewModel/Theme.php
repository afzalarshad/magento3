<?php

namespace Rltsquare\Task\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Theme implements ArgumentInterface
{
    public function getAttachmentLink(): string
    {
        return "ViewModel";
    }
}
