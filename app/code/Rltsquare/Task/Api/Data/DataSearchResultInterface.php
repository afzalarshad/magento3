<?php

namespace Rltsquare\Task\Api\Data;

use Rltsquare\Task\Api\Data\DataInterface;

/**
 * Interface DataSearchResultInterface
 * @package Rltsquare\Task\Api\Data
 */
interface DataSearchResultInterface
{
    /**
     * @return Rltsquare\Task\Api\Data\DataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return mixed
     */
    public function setItems(array $items);
}
