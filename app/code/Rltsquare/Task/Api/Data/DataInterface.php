<?php

namespace Rltsquare\Task\Api\Data;

//use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface DataInterface
 * @package Rltsquare\Task\Api\Data
 */
interface DataInterface
{
    /**
     * Entity ID.
     */
    const ENTITY_ID = 'entity_id';

    /**
     * Created-at timestamp.
     */
    const CREATED_AT = 'created_at';

    /**
     * Updated-at timestamp.
     */
    const UPDATED_AT = 'updated_at';

    /**
     * employee_name varchar
     */
    const NAME = 'employee_name';
    /**
     * employee_email varchar
     */
    const EMAIL = 'employee_email';
    /**
     * employee_designation varchar
     */
    const DESIGNATION = 'employee_designation';
    /**
     * employee_status varchar
     */
    const STATUS = 'employee_status';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param $entityId
     * @return \Rltsquare\Task\Api\Data\DataInterface
     */
    public function setEntityId($entityId);

    /**
     * @return string|null
     */
    public function getEmployeeName();

    /**
     * @param $employeeName
     * @return void
     */
    public function setEmployeeName($employeeName);

    /**
     * @return string|null
     */
    public function getEmployeeEmail();

    /**
     * @param $employeeEmail
     * @return $this
     */
    public function setEmployeeEmail($employeeEmail);

    /**
     * @return string|null
     */
    public function getEmployeeDesignation();

    /**
     * @param $employeeDesignation
     * @return $this
     */
    public function setEmployeeDesignation($employeeDesignation);

    /**
     * @return string|null
     */
    public function getEmployeeStatus();

    /**
     * @param $employeeStatus
     * @return $this
     */
    public function setEmployeeStatus($employeeStatus);

    /**
     * @return string|null Created-at timestamp.
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt timestamp
     * @return $this
     */
    public function setCreatedAt($createdAt);

//    /**
//     * @return \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface|null
//     */
//    public function getExtensionAttributes();
//
//    /**
//     * @param \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//     * @return void
//     */
//    public function setExtensionAttributes(
//        \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//    );
}
