<?php

namespace Rltsquare\Task\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Rltsquare\Task\Api\Data\DataInterface;
use Rltsquare\Task\Api\Data\DataSearchResultInterface;

/**
 * Interface DataRepositoryInterface
 * @package Rltsquare\Task\Api
 */
interface DataRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param $email
     * @return DataInterface
     * @throws NoSuchEntityException
     */
    public function getByEmail($email);

    /**
     * @param DataInterface $RLTEmployee
     * @return mixed
     * @throws CouldNotSaveException
     */
    public function save(DataInterface $RLTEmployee);

    /**
     * @param DataInterface $RLTEmployee
     * @return mixed
     * @throws CouldNotDeleteException
     */
    public function delete(DataInterface $RLTEmployee);

    /**
     * @param $id
     * @return mixed
     * @throws CouldNotDeleteException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return DataSearchResultInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
