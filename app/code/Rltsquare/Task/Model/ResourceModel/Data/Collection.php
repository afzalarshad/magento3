<?php

namespace Rltsquare\Task\Model\ResourceModel\Data;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Rltsquare\Task\Api\Data\DataInterface;
use Rltsquare\Task\Model\Data as Model;
use Rltsquare\Task\Model\ResourceModel\Data as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'rlt_employeess_collection';


    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }

}
