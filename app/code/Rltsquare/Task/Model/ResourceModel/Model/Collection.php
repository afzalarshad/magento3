<?php

namespace Rltsquare\Task\Model\ResourceModel\Model;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';
    /**
     * @var string
     */
    protected $_eventPrefix = 'rltsquare_task_model_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'model_collection';

    /**
     * Define the resource model & the model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Rltsquare\Task\Model\Model::class, \Rltsquare\Task\Model\ResourceModel\Model::class);
    }
}
