<?php

namespace Rltsquare\Task\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Data extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'rlt_employeess_resource_model';


    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('rlt_employeess', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}
