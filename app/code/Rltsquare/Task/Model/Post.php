<?php
namespace Rltsquare\Task\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Post extends AbstractModel implements IdentityInterface
{
    /**
     * @const string
     */
    const CACHE_TAG = 'rltsquare_task_post';

    /**
     * @var string
     */
    protected $_cacheTag = 'rltsquare_task_post';

    /**
     * @var string
     */
    protected $_eventPrefix = 'rltsquare_task_post';

    /**
     * @param $tasks_name
     * @return void
     */
    /**
     * @return string[]
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues(): array
    {
        return [];
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Rltsquare\Task\Model\ResourceModel\Post ::class);
    }
}
