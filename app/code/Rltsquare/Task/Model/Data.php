<?php

namespace Rltsquare\Task\Model;

use Magento\Framework\Model\AbstractModel;
use Rltsquare\Task\Api\Data\DataInterface;
use Rltsquare\Task\Model\ResourceModel\Data as ResourceModel;

/**
 * Class Data
 * @package Rltsquare\Task\Model
 */
class Data extends AbstractModel implements DataInterface
{

    const CACHE_TAG = 'rlt_employeess';
    protected $_eventPrefix = 'rlt_employeess';
    protected $_cacheTag = 'rlt_employeess';

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    /**
     * @inheritDoc
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeName($employeeName)
    {
        return $this->setData(self::NAME, $employeeName);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeEmail($employeeEmail)
    {
        return $this->setData(self::EMAIL, $employeeEmail);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeDesignation()
    {
        return $this->getData(self::DESIGNATION);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeDesignation($employeeDesignation)
    {
        return $this->setData(self::DESIGNATION, $employeeDesignation);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeStatus($employeeStatus)
    {
        return $this->setData(self::STATUS, $employeeStatus);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
