<?php

namespace Rltsquare\Task\Model;

use Magento\Framework\Api\SearchResults;
use Rltsquare\Task\Api\Data\DataSearchResultInterface;

class DataSearchResult extends SearchResults implements DataSearchResultInterface
{
}
