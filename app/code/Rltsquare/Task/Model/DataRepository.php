<?php

namespace Rltsquare\Task\Model;

use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Rltsquare\Task\Api\Data\DataInterface;
use Rltsquare\Task\Api\Data\DataSearchResultInterfaceFactory;
use Rltsquare\Task\Api\DataRepositoryInterface;
use Rltsquare\Task\Model\ResourceModel\Data as ResourceModelRLTEmployee;
use Rltsquare\Task\Model\ResourceModel\Data\Collection;
use Rltsquare\Task\Model\ResourceModel\Data\CollectionFactory;

/**
 * Class RLTEmployeeRepository
 *  Rltsquare\Task\Model
 */
class DataRepository implements DataRepositoryInterface
{
    /** @var CollectionFactory */
    private CollectionFactory $collectionFactory;

    /** @var DataSearchResultInterfaceFactory */
    private DataSearchResultInterfaceFactory $employeeSearchResultInterfaceFactory;

    /** @var DataFactory */
    private DataFactory $employeeFactory;

    /** @var ResourceModelRLTEmployee */
    private ResourceModelRLTEmployee $resourceModelRLTEmployee;

    /**
     * RLTEmployeeRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param DataSearchResultInterfaceFactory $employeeSearchResultInterfaceFactory
     * @param DataFactory $employeeFactory
     * @param ResourceModelRLTEmployee $resourceModelRLTEmployee
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        DataSearchResultInterfaceFactory $employeeSearchResultInterfaceFactory,
        DataFactory $employeeFactory,
        ResourceModelRLTEmployee $resourceModelRLTEmployee
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->employeeSearchResultInterfaceFactory = $employeeSearchResultInterfaceFactory;
        $this->employeeFactory = $employeeFactory;
        $this->resourceModelRLTEmployee = $resourceModelRLTEmployee;
    }

    /**
     * @inheritDoc
     */
    public function getByEmail($email)
    {
        $rltEmployee = $this->employeeFactory->create();
        $this->resourceModelRLTEmployee->load($rltEmployee, $email, 'employee_email');
        if (! $rltEmployee) {
            throw new NoSuchEntityException(
                __('Object with email "%1" already exists.', $email)
            );
        }
        return $rltEmployee;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id)
    {
        try {
            $this->delete($this->getById($id));
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
    }

    /**
     * @inheritDoc
     */
    public function delete(DataInterface $object)
    {
        try {
            $this->resourceModelRLTEmployee->delete($object);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        $rltEmployee = $this->employeeFactory->create();
        $this->resourceModelRLTEmployee->load($rltEmployee, $id);
        if (! $rltEmployee) {
            throw new NoSuchEntityException(
                __('Object with id "%1" does not exist.', $id)
            );
        }
        return $rltEmployee;
    }

    /**
     * @inheritDoc
     */
    public function save(DataInterface $RLTEmployee)
    {
        try {
            $this->resourceModelRLTEmployee->save($RLTEmployee);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->employeeSearchResultInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
