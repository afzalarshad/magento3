<?php

namespace Rltsquare\Task\Controller\Image;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Rltsquare\Task\Model\ModelFactory;
use Rltsquare\Task\Model\ResourceModel\Model as ModelResource;

class Save implements ActionInterface
{

    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * Undocumented variable
     *
     * @var ManagerInterface [type]
     */
    protected ManagerInterface $messageManager;
    /**
     * @var UploaderFactory
     */
    protected UploaderFactory $uploaderFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlInterface;

    /**
     * @var AdapterFactory
     */
    protected AdapterFactory $adapterFactory;
    /**
     * @var ModelFactory
     */
    protected ModelFactory $modelFactory;
    /**
     * @var Filesystem
     */
    protected Filesystem $filesystem;
    /**
     * @var RedirectInterface
     */
    protected RedirectInterface $redirect;

    /**
     * @var Http
     */
    protected Http $request;
    /**
     * @var ResponseFactory
     */
    protected ResponseFactory $responseFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $url;
    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $resultRedirect;
    /**
     * @var ResultFactory
     */
    protected ResultFactory $resultFactory;
    /**
     * @var ModelResource
     */
    protected ModelResource $resource;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ModelFactory $modelFactory
     * @param UploaderFactory $uploaderFactory
     * @param AdapterFactory $adapterFactory
     * @param Filesystem $filesystem
     * @param ResultFactory $resultFactory
     * @param Http $request
     * @param RedirectInterface $redirect
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlInterface
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     * @param RedirectFactory $resultRedirect
     * @param ModelResource $resource
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ModelFactory $modelFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        ResultFactory $resultFactory,
        Http $request,
        RedirectInterface $redirect,
        ManagerInterface $messageManager,
        UrlInterface $urlInterface,
        ResponseFactory $responseFactory,
        UrlInterface $url,
        RedirectFactory $resultRedirect,
        ModelResource $resource
    ) {
        $this->pageFactory = $pageFactory;
        $this->modelFactory = $modelFactory;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->messageManager = $messageManager;
        $this->urlInterface = $urlInterface;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->resultRedirect = $resultRedirect;
        $this->resource = $resource;
    }

    /**
     * View page action
     *
     * @return ResultInterface
     * @throws Exception
     */
    public function execute(): ResultInterface
    {
        $data = $this->request->getParams();
        $files = $this->request->getFiles();
        if (isset($files['image']['name']) && $files['image']['name'] != '') {
            $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
            $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $imageAdapter = $this->adapterFactory->create();
            $uploaderFactory->addValidateCallback('custom_image_upload', $imageAdapter, 'validateUploadFile');
            $uploaderFactory->setAllowRenameFiles(true);
            $uploaderFactory->setFilesDispersion(true);
            $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
            $destinationPath = $mediaDirectory->getAbsolutePath('images');
            $result = $uploaderFactory->save($destinationPath);
            if (! $result) {
                throw new LocalizedException(
                    __('File cannot be saved to path: $1', $destinationPath)
                );
            }
            $imagePath = 'images' . $result['file'];
            $data['image'] = $imagePath;

        }
        $model = $this->modelFactory->create();
        $model->setData($data);
        if ($this->resource->save($model)) {
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        } else {
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }

        $resultRedirect = $this->resultRedirect->create();
        $resultRedirect->setPath('rltsquare/image/index');
        return $resultRedirect;
    }
}
