<?php

namespace Rltsquare\Task\Controller\Image;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Rltsquare\Task\Model\ModelFactory;

class Index implements ActionInterface
{
    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * @var UploaderFactory
     */
    protected UploaderFactory $uploaderFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlInterface;
    /**
     * @var AdapterFactory
     */
    protected AdapterFactory $adapterFactory;
    /**
     * @var Filesystem
     */
    protected Filesystem $filesystem;
    /**
     * @var ResponseFactory
     */
    protected ResponseFactory $responseFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $url;
    /**
     * @var PageFactory
     */

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ModelFactory $modelFactory
     * @param UploaderFactory $uploaderFactory
     * @param AdapterFactory $adapterFactory
     * @param Filesystem $filesystem
     * @param UrlInterface $urlInterface
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ModelFactory $modelFactory,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        UrlInterface $urlInterface,
        ResponseFactory $responseFactory,
        UrlInterface $url
    ) {
        $this->pageFactory = $pageFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->urlInterface = $urlInterface;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {

        return $this->pageFactory->create();
    }
}
