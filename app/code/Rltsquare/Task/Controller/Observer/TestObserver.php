<?php

namespace Rltsquare\Task\Controller\Observer;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Rltsquare\Task\Observer\SalesOrder;
use Magento\Framework\View\Result\PageFactory;

class TestObserver implements HttpGetActionInterface
{
    public SalesOrder $observer;

    /**
     * @var EventManager
     */
    private EventManager $eventManager;
    public function __construct(
        EventManager $eventManager,
        SalesOrder $observer
    ) {
        $this->eventManager = $eventManager;
        $this->observer = $observer;
    }
    /**
     * Execute action based on request and return result
     *
     * @return void
     */
    public function execute()
    {
        $textDisplay = new DataObject(['text' => 'Controller Event']);
        $this->eventManager->dispatch('displaying_text', ['mp_text' => $textDisplay]);
        echo $textDisplay->getText();
        exit;
    }
}
