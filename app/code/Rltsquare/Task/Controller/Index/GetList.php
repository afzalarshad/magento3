<?php
declare(strict_types=1);

namespace Rltsquare\Task\Controller\Index;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\View\Result\PageFactory;

class GetList implements ActionInterface
{
    /** @var PageFactory */
    private PageFactory $pageFactory;

    /**
     * EmployeeRegistrationForm constructor.
     * @param PageFactory $pageFactory
     */
    public function __construct(
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $pageFactory = $this->pageFactory->create();
        $pageFactory->getConfig()->getTitle()->set('RLTSquare Employee List');
        return $pageFactory;
    }
}
