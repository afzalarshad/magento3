<?php
declare(strict_types=1);

namespace Rltsquare\Task\Controller\Index;

use Exception;
use Laminas\Mvc\Controller\Plugin\Service\ForwardFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Rltsquare\Task\Api\Data\DataInterface;
use Rltsquare\Task\Api\DataRepositoryInterface;
use Rltsquare\Task\Model\DataFactory;

/**
 * Class SubmitForm
 * @package Rltsquare\Task\Controller\Index
 */
class SubmitForm implements ActionInterface
{
    /**
     * @var RedirectInterface
     */
    protected RedirectInterface $redirect;
    /**
     * @var Http
     */
    protected Http $request;
    /**
     * @var ResponseFactory
     */
    protected ResponseFactory $responseFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $url;
    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $resultRedirect;
    /**
     * @var ResultFactory
     */
    protected ResultFactory $resultFactory;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlInterface;
    /**
     * Undocumented variable
     *
     * @var ManagerInterface [type]
     */
    protected ManagerInterface $messageManager;
    /**
     * @var ForwardFactory
     */
    protected ForwardFactory $resultForwardFactory;
    /**
     * @var ResultRedirectFactory
     */
    protected $resultRedirectFactory;
    /**
     * @var
     */
    /** @var Validator */
    private Validator $formKeyValidator;
    /** @var UrlInterface */
    private UrlInterface $urlModel;
    /** @var DataFactory */
    private DataFactory $rltEmployeeFactory;
    /** @var DataRepositoryInterface */
    private DataRepositoryInterface $rltEmployeeRepository;

    /**
     * SubmitForm constructor.
     * @param UrlInterface $urlModel
     * @param Validator $formKeyValidator
     * @param DataFactory $rltEmployeeFactory
     * @param ResultFactory $resultFactory
     * @param Http $request
     * @param RedirectInterface $redirect
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlInterface
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     * @param RedirectFactory $resultRedirect
     * @param ForwardFactory $resultForwardFactory
     * @param DataRepositoryInterface $rltEmployeeRepository
     */
    public function __construct(
        UrlInterface $urlModel,
        Validator $formKeyValidator,
        DataFactory $rltEmployeeFactory,
        ResultFactory $resultFactory,
        Http $request,
        RedirectInterface $redirect,
        ManagerInterface $messageManager,
        UrlInterface $urlInterface,
        ResponseFactory $responseFactory,
        UrlInterface $url,
        RedirectFactory $resultRedirect,
        ForwardFactory $resultForwardFactory,
        DataRepositoryInterface $rltEmployeeRepository
    ) {
        $this->urlModel = $urlModel;
        $this->formKeyValidator = $formKeyValidator;
        $this->rltEmployeeFactory = $rltEmployeeFactory;
        $this->rltEmployeeRepository = $rltEmployeeRepository;
        $this->messageManager = $messageManager;
        $this->urlInterface = $urlInterface;
        $this->redirect = $redirect;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->resultRedirect = $resultRedirect;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create();
        if (! $this->request->isPost()
            || ! $this->formKeyValidator->validate($this->request)
        ) {
            $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
            $this->messageManager->addErrorMessage(
                __('Invalid Form Key, Please Refresh the Page')
            );
            return $this->resultRedirect->create()
                ->setUrl($this->redirect->error($url));
        }

        $employeeEmail = $this->request->getParam(DataInterface::EMAIL);
        try {
            $this->isEmailExists($employeeEmail);
            $requestData = $this->request->getParams();

            $employObject = $this->rltEmployeeFactory->create();
            $employObject->setEmployeeName($requestData['employee_name']);
            $employObject->setEmployeeEmail($requestData['employee_email']);
            $employObject->setEmployeeDesignation($requestData['employee_designation']);
            $employObject->setEmployeeStatus($requestData['employee_status']);

            $this->rltEmployeeRepository->save($employObject);
            $this->messageManager->addSuccessMessage(
                __('Employee with email %1 saved successfully.', $employeeEmail)
            );
            $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
            $resultRedirect->setUrl($this->redirect->success($url));
            return $resultRedirect;

            /**
             * Do not use this way of setting the data for an entity.
             */
//            $employObject->setData($this->getRequest()->getParams());
//            if ($employObject->save()) {
//                $this->messageManager->addSuccessMessage(
//                    __('Employee with email %1 saved successfully.', $employeeEmail)
//                );
//                $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
//                $resultRedirect->setUrl($this->_redirect->success($url));
//                return $resultRedirect;
//            }
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while saving the data of employee: ' . $exception->getMessage())
            );
        }

        $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
        $resultRedirect->setUrl($this->redirect->error($url));
        return $resultRedirect;
    }

    /**
     * @param $email
     * @return void
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     */
    private function isEmailExists($email): void
    {
        try {
            $employee = $this->rltEmployeeRepository->getByEmail($email);
            if ($employee->getEntityId() !== null) {
                throw new AlreadyExistsException(__('Employee with this email already exists.'));
            } else {
                return;
            }
        } catch (AlreadyExistsException $alreadyExistsException) {
            throw new AlreadyExistsException(__($alreadyExistsException->getMessage()));
        }
    }
}
