<?php

namespace Rltsquare\Task\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class EmployeeRegistrationForm
 * @package Rltsquare\Task\Controller\Index
 */
class EmployeeRegistrationForm implements ActionInterface
{
    /** @var PageFactory */
    private PageFactory $pageFactory;

    /**
     * EmployeeRegistrationForm constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
//        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $pageFactory = $this->pageFactory->create();
        $pageFactory->getConfig()->getTitle()->set('RLTSquare Employee Form');
        return $pageFactory;
    }
}
