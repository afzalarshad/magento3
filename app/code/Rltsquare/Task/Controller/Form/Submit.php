<?php

namespace Rltsquare\Task\Controller\Form;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\PageFactory;
use Rltsquare\Task\Model\PostFactory;
use Rltsquare\Task\Model\ResourceModel\Post as PostResource;

class Submit implements HttpPostActionInterface
{
    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * @var PostFactory
     */
    protected PostFactory $postFactory;
    /**
     * @var
     */
    /**
     * @var Http
     */
    protected Http $request;

    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $resultRedirect;
    /**
     * @var ResultFactory
     */
    protected ResultFactory $resultFactory;
    /**
     * @var PostResource
     */
    protected PostResource $resource;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param PostFactory $postFactory
     * @param Http $request
     * @param ResultFactory $resultFactory
     * @param RedirectInterface $redirect
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlInterface
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     * @param RedirectFactory $resultRedirect
     * @param PostResource $resource
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        PostFactory $postFactory,
        Http $request,
        ResultFactory $resultFactory,
        RedirectInterface $redirect,
        ManagerInterface $messageManager,
        UrlInterface $urlInterface,
        ResponseFactory $responseFactory,
        UrlInterface $url,
        RedirectFactory $resultRedirect,
        PostResource $resource
    ) {
        $this->pageFactory = $pageFactory;
        $this->postFactory = $postFactory;
        $this->request = $request;
        $this->resultRedirect = $resultRedirect;
        $this->resultFactory = $resultFactory;
        $this->resource = $resource;
    }

    /**
     * View page action
     *
     * @return ResultInterface
     * @throws Exception
     */
    public function execute(): ResultInterface
    {
        $data = (array)$this->request->getParams();
        $model = $this->postFactory->create();
        $model->setData($data);
        $this->resource->save($model);

        $resultRedirect = $this->resultRedirect->create();
        $resultRedirect->setPath('rltsquare/form/form');
        return $resultRedirect;
    }
}
