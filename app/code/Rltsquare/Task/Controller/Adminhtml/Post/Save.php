<?php

namespace Rltsquare\Task\Controller\Adminhtml\Post;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Rltsquare\Task\Model\PostFactory;
use Rltsquare\Task\Model\ResourceModel\Post as PostResource;

class Save extends Action
{
    /**
     * @var PostFactory
     */
    public PostFactory $postFactory;
    /**
     * @var PostResource
     */
    protected PostResource $resource;

    /**
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PostResource $resource
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PostResource $resource
    ) {
        $this->postFactory = $postFactory;
        $this->resource = $resource;
        parent::__construct($context);
    }

    /**
     * @throws Exception
     */
    public function execute()
    {

        $post = $this->postFactory->create();
        $data = (array)$this->getRequest()->getPost('tasks');
        $post->setData($data);
        $this->resource->save($post);
        return $this->resultRedirectFactory->create()->setPath('rltsquare_task/post/index');
    }
}
