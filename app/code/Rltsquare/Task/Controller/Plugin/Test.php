<?php

declare(strict_types=1);

namespace Rltsquare\Task\Controller\Plugin;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;

/**
 * Rltsquare_Test
 *
 * plugins, events
 */
class Test implements ActionInterface
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        echo "Hello World";
        echo "<br />";
        $this->testBeforePlugin(1);
        echo "<br />";
        exit;
    }

    /**
     * @param int $a
     * @return int
     */
    public function testBeforePlugin(int $a): int
    {
        echo "testBeforePlugin Called";
        echo "<br />";
        return $a;
    }
}
