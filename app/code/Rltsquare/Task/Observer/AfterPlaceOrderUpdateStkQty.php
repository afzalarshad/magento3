<?php

declare(strict_types=1);

namespace Rltsquare\Task\Observer;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Validation\ValidationException;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\Sales\Model\Order;

/**
 * After Place Order Update
 */
class AfterPlaceOrderUpdateStkQty implements ObserverInterface
{
    /**
     * @var SourceItemsSaveInterface
     */
    protected SourceItemsSaveInterface $sourceItemsSave;

    /**
     * @var SourceItemInterfaceFactory
     */
    protected SourceItemInterfaceFactory $sourceItemFactory;

    /**
     * @var Order
     */
    protected Order $order;

    /**
     * @var Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @param Order $order
     * @param StockRegistryInterface $stockRegistry
     * @param SourceItemsSaveInterface $sourceItemsSave
     * @param SourceItemInterfaceFactory $sourceItemFactory
     */
    public function __construct(
        Order $order,
        StockRegistryInterface $stockRegistry,
        SourceItemsSaveInterface $sourceItemsSave,
        SourceItemInterfaceFactory $sourceItemFactory
    ) {
        $this->order = $order;
        $this->stockRegistry = $stockRegistry;
        $this->sourceItemsSave = $sourceItemsSave;
        $this->sourceItemFactory = $sourceItemFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getId();
//        $sourceItem = $this->sourceItemFactory->create();
        if ($order->getState() == "new") {
            foreach ($order->getAllVisibleItems() as $item) {
                $productSku = $item->getSku();
                $QtyOrdered = $item->getQtyOrdered();
                $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
                $stockQty = $stockItem->getQty() - $QtyOrdered;
                $stockItem->setQty($stockQty);
                $stockItem->setIsInStock((bool)$stockQty);
                $this->stockRegistry->updateStockItemBySku($productSku, $stockItem);
            }
        }
    }
}
