<?php
declare(strict_types=1);

namespace Rltsquare\Task\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SalesOrder implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {

        $order = $observer->getEvent()->getOrder()
            ->setCustomerFirstname('test')
            ->setCustomerLastname('test')
            ->setCustomerEmail('test@gmail.com');
        $order->save();
    }
}
