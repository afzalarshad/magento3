<?php

declare(strict_types=1);

namespace Rltsquare\Task\Observer;

use Magento\Catalog\Model\Product as Product;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Validation\ValidationException;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;

/**
 * After Place Order Update Stock Quantity and Salable Quantity
 */
class AfterPlaceOrderUpdateQty implements ObserverInterface
{

    /**
     * @var SourceItemInterface
     */
    protected SourceItemInterface $sourceItemInterface;
    /**
     * @var SourceItemsSaveInterface
     */
    protected SourceItemsSaveInterface $sourceItemsSave;
    /**
     * @var Product
     */
    protected Product $product;
    /**
     * @var StockItemRepository
     */
    protected StockItemRepository $stockRepository;
    /**
     * @var GetSalableQuantityDataBySku
     */
    protected GetSalableQuantityDataBySku $salableQty;

    /**
     * @param GetSalableQuantityDataBySku $salableQty
     * @param SourceItemsSaveInterface $sourceItemsSave
     * @param SourceItemInterface $sourceItemInterface
     * @param StockItemRepository $stockRepository
     * @param Product $product
     */
    public function __construct(
        GetSalableQuantityDataBySku $salableQty,
        SourceItemsSaveInterface $sourceItemsSave,
        SourceItemInterface $sourceItemInterface,
        StockItemRepository $stockRepository,
        Product $product
    ) {
        $this->sourceItemsSave = $sourceItemsSave;
        $this->sourceItemInterface = $sourceItemInterface;
        $this->product = $product;
        $this->stockRepository = $stockRepository;
    }

    /**
     * @param $sku
     * @param $qty
     * @return AfterPlaceOrderUpdateQty
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws ValidationException
     */
    public function setQtyToProduct($sku, $qty): AfterPlaceOrderUpdateQty
    {
        $product = $this->product->getIdBySku($sku);
        $productStock = $this->stockRepository->get($product);
        $productQty = $productStock->getQty();
        $totalQty = $productQty - $qty;
        $this->sourceItemInterface->setSku($sku);
        $this->sourceItemInterface->setQuantity($totalQty);
        $this->sourceItemInterface->setStatus(1);
        $this->sourceItemInterface->setSourceCode('default');
        $this->sourceItemsSave->execute([$this->sourceItemInterface]);
        return $this;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws ValidationException
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        foreach ($order->getAllVisibleItems() as $item) {
            $productSku = $item->getSku();
            $productQty = $item->getQtyOrdered();
            $this->setQtyToProduct($productSku, $productQty);
        }
    }
}
