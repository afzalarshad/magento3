<?php
declare(strict_types=1);

namespace Rltsquare\Task\Ui\Component\MassAction\Status;

use JsonSerializable;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;

class Options implements JsonSerializable
{
    /**
     * @var $options
     */
    protected $options;
    /**
     * @var array
     */
    protected array $data;
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;
    /**
     * @var $urlPath
     */
    protected $urlPath;
    /**
     * @var $paramName
     */
    protected $paramName;
    /**
     * @var array
     */
    protected array $additionalData = [];

    /**
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        array        $data = []
    ) {
        $this->data = $data;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        if ($this->options === null) {
            $options = [[
                'value' => '1',
                'label' => 'Active'
            ], [
                'value' => '0',
                'label' => 'Inactive'
            ]];
            $this->prepareData();
            foreach ($options as $optionCode) {
                $this->options[$optionCode['value']] = [
                    'type' => 'change_status_' . $optionCode['value'],
                    'label' => __($optionCode['label']),
                    '__disableTmpl' => true
                ];

                if ($this->urlPath && $this->paramName) {
                    $this->options[$optionCode['value']]['url'] = $this->urlBuilder->getUrl(
                        $this->urlPath,
                        [$this->paramName => $optionCode['value']]
                    );
                }

                $this->options[$optionCode['value']] = array_merge_recursive(
                    $this->options[$optionCode['value']],
                    $this->additionalData
                );
            }
            $this->options = array_values($this->options);
        }

        return $this->options;
    }

    /**
     * @return void
     */
    protected function prepareData()
    {
        foreach ($this->data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->urlPath = $value;
                    break;
                case 'paramName':
                    $this->paramName = $value;
                    break;
                case 'confirm':
                    foreach ($value as $messageName => $message) {
                        $this->additionalData[$key][$messageName] = (string)new Phrase($message);
                    }
                    break;
                default:
                    $this->additionalData[$key] = $value;
                    break;
            }
        }
    }
}
