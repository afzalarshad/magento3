<?php

declare(strict_types=1);

namespace Rltsquare\Task\Plugin;

use Rltsquare\Task\Controller\Plugin\Test;

class Plugin
{
    /**
     * @param Test $subject
     * @param int $a
     * @return int[]
     */
    public function beforeTestBeforePlugin(
        Test $subject,
        int $a
    ) {
        echo "beforeTestBeforePlugin called";
        echo "<br />";
        $a = 2;
        return [$a];
    }

    /**
     * @param Test $subject
     * @param $result
     * @return int
     */
    public function afterTestBeforePlugin(
        Test $subject,
        $result
    ): int {
        echo $result + 1;
        echo "<br />";
        return $result + 1;
    }

    /**
     * @param Test $subject
     * @param callable $proceed
     * @param ...$args
     * @return mixed
     */
    public function aroundTestBeforePlugin(
        Test $subject,
        callable $proceed,
        ...$args
    ) {
        $firstArgument = $args[0];
        echo $firstArgument;
        echo "<br />";
        return $proceed($firstArgument);

        $temp = null;
    }
}
