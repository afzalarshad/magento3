<?php
declare(strict_types=1);

namespace Rltsquare\Task\Plugin;

use Magento\Sales\Model\ResourceModel\Order;

/**
 * Change Order Value Before Save
 */
class Sales
{
    /**
     * @param Order $subject
     * @param $result
     * @return array
     */
    public function beforeSave(
        Order $subject,
        $result
    ): array {
        if (! $result || ! $result->getId()) {
            $result->setCustomerEmail('Test@gmail.com');
            $result->setCustomerFirstname('Test');
            $result->setCustomerLastname('test');
        }
        return [$result];
    }
}
