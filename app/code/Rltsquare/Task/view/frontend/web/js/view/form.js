/*global define*/
define([
    'Magento_Ui/js/form/form',
    'Magento_Checkout/js/model/step-navigator'

], function (Component,stepNavigator) {
    'use strict';
    return Component.extend({
        initialize: function () {
            this._super();
            // component initialization logic
            return this;
        },

        /**
         * Form submit handler
         *
         * This method can have any name.
         */
        onSubmit: function () {
            // trigger form validation
            this.source.set('params.invalid', false);
            this.source.trigger('customCheckoutForm.data.validate');

            // verify that form data is valid
            if (!this.source.get('params.invalid')) {
                // data is retrieved from data provider by value of the customScope property
                let formData = this.source.get('customCheckoutForm');
                // do something with form data
                console.dir(formData);
                stepNavigator.next();
            }
        }
    });
});
