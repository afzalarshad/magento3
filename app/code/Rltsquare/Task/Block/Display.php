<?php

namespace Rltsquare\Task\Block;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Rltsquare\Task\Model\PostFactory;
use Rltsquare\Task\Model\ResourceModel\Post\CollectionFactory;

class Display extends Template
{
    /**
     * @var PostFactory
     */
    protected PostFactory $postFactory;

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;

    /**
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        collectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     */
    public function getPostCollection()
    {
        return $this->collectionFactory->create();
    }
}
