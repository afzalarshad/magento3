<?php

namespace Rltsquare\Task\Block\RLTEmployee;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Rltsquare\Task\Api\DataRepositoryInterface;

class GetList extends \Magento\Framework\View\Element\Template
{
    /** @var SearchCriteriaBuilder */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /** @var FilterBuilder */
    private FilterBuilder $filterBuilder;

    /** @var DataRepositoryInterface */
    private DataRepositoryInterface $dataRepository;

    public function __construct(
        Template\Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        DataRepositoryInterface $dataRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->dataRepository = $dataRepository;
    }

    public function getEmployeeList()
    {
        $filters[] = $this->filterBuilder
            ->setField('employee_email')
            ->setConditionType('like')
            ->setValue('%afzel%')
            ->create();

        $this->searchCriteriaBuilder->addFilters($filters);

        /** @var SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->dataRepository->getList($searchCriteria);
        return $result->getItems();
    }
}
