<?php

namespace Rltsquare\Task\Block\RLTEmployee;

use Magento\Framework\View\Element\Template;

/**
 * Class RegistrationForm
 * @package Rltsquare\Task\Block\RLTEmployee
 */
class RegistrationForm extends Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSubmitFormUrl(): string
    {
        return $this->getUrl('service/index/submitform');
    }
}
