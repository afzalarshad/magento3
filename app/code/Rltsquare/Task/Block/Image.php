<?php

namespace Rltsquare\Task\Block;

use Magento\Framework\View\Element\Template\Context;
use Rltsquare\Task\Model\ModelFactory;
use Rltsquare\Task\Model\ResourceModel\Model\Collection;
use Rltsquare\Task\Model\ResourceModel\Model\CollectionFactory;

class Image extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var ModelFactory
     */
    protected ModelFactory $modelFactory;

    /**
     * @param Context $context
     * @param ModelFactory $modelFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        ModelFactory $modelFactory,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return Collection
     */
    public function getImageData(): Collection
    {
        return $this->collectionFactory->create();
    }
}
