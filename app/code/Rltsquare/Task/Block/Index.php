<?php

namespace Rltsquare\Task\Block;

use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    public function getCompany(): Phrase
    {
        return __('RLTSquare Tasks');
    }

    public function getName(): Phrase
    {
        return __('Afzel Arshad');
    }

    public function getImage(): string
    {
        return $this->getViewFileUrl('Rltsquare_Task::images/1.jpg');
    }

    public function getEducation(): Phrase
    {
        return __('Bachelour In Computer Science');
    }

    public function getDOB(): Phrase
    {
        return __('01/09/1996');
    }

    public function getLinkedInProfile(): Phrase
    {
        return __('https://www.linkedin.com/in/afzel-arshad-91b761117/');
    }
}
